In this directory is a minimal subset designed to test that the app fulfills the requirements for the rubric.

The subset contains 4 restaurants, 3 from the city Las Vegas and one from phoenix. All the restaurants share the category "restaurants". Two restaurants from Las Vegas have a stars rating of 4.5 with Veggie House having more reviews. They have quite different open times. The third has a stars rating of 4. The final restaurant is based in Phoenix. Note that up to 5 pictures should be returned (for restaurants that have pictures) and up to 5 recommendations should be returned when enough information about users and reviews for that restaurant is available in the subset. There are pictures for Veggie House and Fat Moe's Pizza & Wings in the subset. There are 2 reviews for Wetzel's Pretzels in the file review_test.json with one having a useful score of 1 and the other 0.

Test cases:

	1) Food category: Restaurants, City: Las Vegas, Hours: Monday 14:00. Should return: Veggie House, Shows that the app returns restaurants first by stars and then review count.

	2) Food category: Burger, City: Las Vegas, Hours: Monday: 14:00, Should return: Fat Moe's Pizza & Wings, Shows that the app returns restaurants with the selected cuisine.

	3) Food category: Restaurants, City: Las Vegas, Hours: Friday 23:00, Should return: Mama Napoli Pizza, Shows that the app returns restaurants within the selected timeframe.

	4) Food category: Restaurants, City Phoenix, Hours: Monday 14:00, Should return: Wetzel's Pretzels with review from Dolores, Shows that the app returns restaurants from the selected city and the most useful review is returned.
