from flask import Flask, render_template, redirect, request, url_for
from py2neo import Graph, Node, Relationship
import json
import datetime as datetime
import calendar

app = Flask(__name__)
graph = Graph("http://neo4j:neo4j@localhost:8001", password="0000")
graph.begin()
foods = graph.run("MATCH(c:Cuisine) WHERE size((c)<-[:HAS]-(:Restaurant)) >= 1 return c.name as name order by name asc").data()
cities = graph.run("MATCH (r:Restaurant) RETURN DISTINCT r.city AS city order by city asc").data()
cities = [i["city"] for i in cities]

##### recomendation feature incomplete

@app.route('/', defaults={'u_path': ''}, methods=["GET"])
@app.route('/<path:u_path>')
def final(u_path):
    food = request.args.get("food")
    city = request.args.get("city")
    date_chosen = request.args.get("date")
    time_chosen = request.args.get("time")
    print(date_chosen)
    print(time_chosen)
    if city != None:
        if (len(city) == 0): # If no city is chosen
            restaurant = {"name":"Select a city","review_count":"","stars":0,"address":"","id":""}
            review = {"author":"","text":"","stars":0,"date":""}
            reco = {"place1":"No data found","place2":"...","place3":"...","place4":"...","place5":"..."}
            pics = ["no_image", ""]
            return render_template("Foodar_v1.html",restaurant=restaurant, review=review, cities=cities, cuisines=foods, reco=reco, pics=pics)
        if ((date_chosen == "") or (time_chosen == "")):
            restaurant = graph.run("MATCH (r:Restaurant{city:\""+city+"\"})-[:HAS]-(c:Cuisine{name:\""+food+"\"})" \
            " RETURN r.id, r.name AS name, r.city AS city, r.address AS address, r.stars AS stars, r.review_count AS review_count ORDER BY r.stars DESC, r.review_count DESC").data()
        else:
            # find the day of the week of the chosen date
            year = int(date_chosen[0:4])
            month = int(date_chosen[5:7])
            day_num = int(date_chosen[8:10])
            day = datetime.date(year, month, day_num)
            day = day.weekday()
            day = calendar.day_name[day]
            # day found
            restaurant = graph.run("MATCH (r:Restaurant{city:\""+city+"\"})-[:HAS]-(c:Cuisine{name:\""+food+"\"}) " \
            "WHERE left(r."+day+", 5) <= \""+time_chosen+"\" AND right(r."+day+", 5) > \""+time_chosen+"\" " \
            "RETURN r.id, r.name AS name, r.city AS city, r.address AS address, r.stars AS stars, r.review_count AS review_count ORDER BY r.stars DESC, r.review_count DESC").data()
        if restaurant != []: # If a restaurant is found
            restaurant = restaurant[0]
            today = datetime.datetime.today()
            date_string = "year: " + str(today.year - 2) + ", month: " + str(today.month) + ", day: " + str(today.day)
            review = graph.run("MATCH(u:User)-[:WROTE]->(r:Review)-[:REVIEW_OF]-(:Restaurant{id:\""+restaurant["r.id"]+"\"}) " \
            "WHERE datetime(substring(r.date,0,10)) >= datetime({"+date_string+"}) RETURN r.text as text, r.stars as stars, " \
            "r.useful as useful, u.id, u.name as author ORDER BY r.useful DESC LIMIT 1").data()
            photos = graph.run("MATCH(p:Photo)-[:PHOTO_OF]->(:Restaurant{id:\""+restaurant["r.id"]+"\"}) RETURN p.id as id, p.caption as caption").data()
            pics = []
            if photos == []:
                pics.append(["no_image", ""])
            else:
                for p in photos:
                    pics.append([p["id"], p["caption"]])

            if review != []:
                review = review[0]
            else:
                review = graph.run("MATCH(u:User)-[:WROTE]->(r:Review)-[:REVIEW_OF]-(:Restaurant{id:\""+restaurant["r.id"]+"\"}) " \
                " RETURN r.text as text, r.stars as stars, r.useful as useful, u.id, u.name as author ORDER BY r.useful DESC LIMIT 1").data()
                if review != []:
                    review = review[0]

            # continue on to populate other recomendations
            if review != []:
                userID = review["u.id"]
                recomendations = graph.run("MATCH (r:Review)<-[:WROTE]-(u:User)-[:FRIENDS*0..1]-()-[:FRIENDS]-(p:User{id:\""+userID+"\"})" \
                " WITH distinct r MATCH (c:Cuisine)<-[:HAS]-(g:Restaurant)<-[:REVIEW_OF]-(r)" \
                " RETURN g.name, sum(r.stars)/count(g.name) AS avg, count(g.name) AS count ORDER BY avg desc, count desc limit 5").data()
                if recomendations != [] :
                    # Recomendations found
                    temp = []
                    for x in recomendations:
                        temp.append(x["g.name"])
                    while len(temp) < 5:
                        temp.append("...")
                    reco = {"place1":temp[0],"place2":temp[1],"place3":temp[2],"place4":temp[3],"place5":temp[4]}
                else:
                    # Recomendations not found
                    reco = {"place1":"No data found","place2":"...","place3":"...","place4":"...","place5":"..."}
            else:
                reco = {"place1":"No data found","place2":"...","place3":"...","place4":"...","place5":"..."}
        else: # If no restaurant is found
            restaurant = {"name":"Nothing found","review_count":"","stars":0,"address":"","id":""}
            review = {"author":"","text":"","stars":0,"date":""}
            reco = {"place1":"No data found","place2":"...","place3":"...","place4":"...","place5":"..."}
            pics = ["no_image", ""]
    else:
        restaurant = {"name":"Nothing found","review_count":"","stars":0,"address":"","id":""}
        review = {"author":"","text":"","stars":0,"date":""}
        reco = {"place1":"No data found","place2":"...","place3":"...","place4":"...","place5":"..."}
        pics = ["no_image", ""]
    return render_template("Foodar_v1.html",restaurant=restaurant, review=review, cities=cities, cuisines=foods, reco=reco, pics=pics)


if __name__ == '__main__':
    app.run(debug=True)
