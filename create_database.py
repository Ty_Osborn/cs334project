# Py file used to create my database. It is super inefficient but gets the job
# done.

from py2neo import Graph, Node, Relationship
import json
import sys

if sys.argv[1] == None:
    review = [json.loads(i) for i in open("./Processed_set/review.json").readlines()]
    user = [json.loads(i) for i in open("./Processed_set/user.json").readlines()]
    business = [json.loads(i) for i in open("./Processed_set/business.json").readlines()]
    food = [i.strip() for i in open("./Processed_set/food_types.txt").readlines()]
    photo = [json.loads(i) for i in open("./Processed_set/photos.json").readlines()]
else:
    review = [json.loads(i) for i in open("./test_set/review_test.json").readlines()]
    user = [json.loads(i) for i in open("./test_set/user_test.json").readlines()]
    business = [json.loads(i) for i in open("./test_set/business_test.json").readlines()]
    food = [i.strip() for i in open("./Processed_set/food_types.txt").readlines()]
    photo = [json.loads(i) for i in open("./test_set/photos_test.json").readlines()]    

graph = Graph("http://neo4j:fnp@localhost:8001")
graph.begin()
graph.run("MATCH (n) DETACH DELETE n")

# Create uniqueness constraints
# graph.run("CREATE CONSTRAINT ON (n:Cuisine) ASSERT n.name IS UNIQUE")
# graph.run("CREATE CONSTRAINT ON (n:Restaurant) ASSERT n.id IS UNIQUE")
# graph.run("CREATE CONSTRAINT ON (n:Review) ASSERT n.id IS UNIQUE")
# graph.run("CREATE CONSTRAINT ON (n:User) ASSERT n.id IS UNIQUE")

# ADD FOOD TYPES

for i in food:
    graph.run("MERGE (n:Cuisine{name:\""+i.strip()+"\"})")

print("Added foods...")

#ADD BUSINESSES

for i in business:
    string = "MERGE (n:Restaurant{id:\""+i["business_id"]+"\",name:\""+i["name"]+"\", address:\""+i["address"]+"\",stars:"+str(i["stars"])+",review_count:"+str(i["review_count"])+"})"
    graph.run(string)

for i in business:
    graph.run("MATCH (r:Restaurant{id:\""+i["business_id"]+"\"}) SET r.city = \""+i["city"]+"\"")

for i in business:
    if not i["hours"] == None:
        for j,k in i["hours"].items():
            graph.run("MATCH (r:Restaurant{id:\""+i["business_id"]+"\"}) SET r."+j+" = \""+k+"\"")


print("Added restuarants...")

#CREATE RELATION BETWEEN FOOD TYPES AND BUSINESSES

for i in business:
    for j in food:
        temp = i["categories"].split(', ')
        if j in temp:
            graph.run("MERGE (n:Restaurant {id: \""+i["business_id"] + "\"}) MERGE (m:Cuisine {name: \""+j+"\"}) MERGE (n)-[:HAS]->(m)")
print("Added HAS relationships...")
#CREATE USERS
for i in user:
    graph.run("MERGE (n:User{id:\""+i["user_id"]+"\" , name:\""+i["name"]+"\"})")

id_list = [i["user_id"] for i in user]
for i in user:
    friend_list = i["friends"].split(', ')
    for friend in friend_list:
        if friend in id_list:
            graph.run("MERGE (n:User{id:\""+i["user_id"]+"\"}) MERGE (m:User{id:\""+friend+"\"}) MERGE (m)<-[:FRIENDS]->(n)")
print("Added Users")

# CREATE REVIEWS AND RELATIONS TO BUSINESS
for j in review:
    if not j["review_id"] == "S--lGYEBiGJFkoOh9DJe8A":
        print("adding reviw id:" + j["review_id"])
        graph.run("MERGE (n:Review{id:\""+j["review_id"]+"\", stars:"+str(j["stars"])+", useful:"+str(j["useful"])+", date:\""+ str(j["date"]) +"\", text:\""+j["text"].replace("\"","\\\"")+"\"}) MERGE (m:Restaurant{id:\""+j["business_id"]+"\"}) MERGE (n)-[:REVIEW_OF]->(m)")
        graph.run("MERGE (r:Review{id:\""+j["review_id"]+"\"}) MERGE (u:User{id:\""+j["user_id"]+"\"}) MERGE (r)<-[:WROTE]-(u)")

# CREATE PHOTOS AND RELATIONS TO BUSINESS
for i in photo:
    graph.run("MERGE (n:Photo {id: \"" + i["photo_id"] + "\", caption:\"" + str(i["caption"]) + "\"}) MERGE (m:Restaurant{id:\""+i["business_id"]+"\"}) MERGE (n)-[:PHOTO_OF]->(m)")
